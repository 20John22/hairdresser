﻿using System.Data.Entity;

using Hairdresser.Entities.Configuration;
using Hairdresser.Model;

namespace Hairdresser.Entities
{
	public class CoreContext : DbContext
	{
		public CoreContext() { }
		public CoreContext(string connectionStringName) : base(connectionStringName) { }

		public DbSet<ProcessTypeInfo> ProcessTypeInfos { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Details> Details { get; set; }


		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Configurations.Add(new ProcessTypeInfoConfiguration());
			modelBuilder.Configurations.Add(new ProductConfiguration());
			modelBuilder.Configurations.Add(new DetailsConfiguration());

			Database.SetInitializer<CoreContext>(null);
		}
	}
}
