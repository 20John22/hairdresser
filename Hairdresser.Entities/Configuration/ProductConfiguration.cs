﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Hairdresser.Model;

namespace Hairdresser.Entities.Configuration
{
	public class ProductConfiguration : EntityTypeConfiguration<Product>
	{
		public ProductConfiguration()
		{
			ToTable("tProduct");
			HasKey(e => e.Id);
			Property(e => e.Id)
				.HasColumnOrder(0)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
				.HasColumnName("Id");
			Property(e => e.Title)
				.HasColumnOrder(1)
				.IsRequired()
				.HasMaxLength(50);
			Property(e => e.Description)
				.HasColumnOrder(2)
				.IsRequired()
				.HasMaxLength(250);
			Property(e => e.Url)
				.HasColumnOrder(3)
				.IsOptional()
				.HasMaxLength(200);
			Property(e => e.MinPrice)
				.HasColumnOrder(4)
				.IsRequired();
			Property(e => e.MaxPrice)
				.HasColumnOrder(5)
				.IsRequired();
			Property(e => e.DetailsId)
				.HasColumnOrder(6);
			HasRequired(e => e.Details)
				.WithMany()
				.HasForeignKey(e => e.DetailsId);
			Property(e => e.ProcessType)
				.HasColumnOrder(7)
				.HasColumnName("ProcessTypeId");
			HasRequired(e => e.ProcessTypeInfo)
				.WithMany()
				.HasForeignKey(e => e.ProcessType);

		}
	}
}
