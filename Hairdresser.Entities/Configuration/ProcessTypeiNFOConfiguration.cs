﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Hairdresser.Model;

namespace Hairdresser.Entities.Configuration
{
	public class ProcessTypeInfoConfiguration : EntityTypeConfiguration<ProcessTypeInfo>
	{
		public ProcessTypeInfoConfiguration()
		{
			ToTable("teProcessType");
			HasKey(e => e.Id);
			Property(e => e.Id)
				.HasColumnOrder(0)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
				.HasColumnName("Id");
			Property(e => e.Code)
				.HasColumnOrder(1)
				.IsRequired()
				.HasMaxLength(50);
			Property(e => e.Name)
				.HasColumnOrder(2)
				.IsRequired()
				.HasMaxLength(80);
		}
	}
}
