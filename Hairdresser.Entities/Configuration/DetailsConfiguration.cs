﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Hairdresser.Model;

namespace Hairdresser.Entities.Configuration
{
	public class DetailsConfiguration : EntityTypeConfiguration<Details>
	{
		public DetailsConfiguration()
		{
			ToTable("tDetails");
			HasKey(e => e.Id);
			Property(e => e.Id)
				.HasColumnOrder(0)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
				.HasColumnName("Id");
			Property(e => e.Title)
				.HasColumnOrder(1)
				.IsRequired()
				.HasMaxLength(150);
			Property(e => e.Content)
				.HasColumnOrder(2)
				.IsOptional()
				.IsMaxLength();
		}
	}
}
