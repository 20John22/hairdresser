namespace Hairdresser.Entities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 150),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.teProcessType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 80),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.tProduct",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Description = c.String(nullable: false, maxLength: 250),
                        Url = c.String(maxLength: 200),
                        MinPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DetailsId = c.Int(nullable: false),
                        ProcessTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.tDetails", t => t.DetailsId, cascadeDelete: true)
                .ForeignKey("dbo.teProcessType", t => t.ProcessTypeId, cascadeDelete: true)
                .Index(t => t.DetailsId)
                .Index(t => t.ProcessTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tProduct", "ProcessTypeId", "dbo.teProcessType");
            DropForeignKey("dbo.tProduct", "DetailsId", "dbo.tDetails");
            DropIndex("dbo.tProduct", new[] { "ProcessTypeId" });
            DropIndex("dbo.tProduct", new[] { "DetailsId" });
            DropTable("dbo.tProduct");
            DropTable("dbo.teProcessType");
            DropTable("dbo.tDetails");
        }
    }
}
