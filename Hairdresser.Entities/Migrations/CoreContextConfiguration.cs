﻿using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Design;
using System.Data.Entity.SqlServer;

namespace Hairdresser.Entities.Migrations
{
	public sealed class CoreContextConfiguration : DbMigrationsConfiguration<CoreContext>
	{
		public CoreContextConfiguration()
		{
			
			AutomaticMigrationsEnabled = false;
			MigrationsDirectory = @"Migrations";
			
			SetSqlGenerator("System.Data.SqlClient", new SqlServerMigrationSqlGenerator());
			CodeGenerator = new CSharpMigrationCodeGenerator();
		}

		protected override void Seed(CoreContext context)
		{
			context.ProcessTypeInfos.AddOrUpdate(
				context.ProcessTypeInfos.Create().Initialize(1, "Koloryzja", "koloryzja"),
				context.ProcessTypeInfos.Create().Initialize(2, "Sytlizacja", "sytlizacja"),
				context.ProcessTypeInfos.Create().Initialize(3, "Kuracja", "kuracja w salonie"),
				context.ProcessTypeInfos.Create().Initialize(4, "ModyfikacjaKoloru", "modyfikacja koloru"),
				context.ProcessTypeInfos.Create().Initialize(5, "TrwalaZmianaFormy", "trwala zmiana formy")
			);
		}
	}
}
