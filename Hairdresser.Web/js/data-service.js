﻿var dataService = angular.module('data-service', []);

dataService.factory('productService', ['$http', '$q',
	function ($http, $q) {
		return {
			getAllProducts: function () {
				var deferred = $q.defer();

			    $http.get('http://hairdresser.apphb.com/api/product')
					.success(function (data, status, headers, config) {
						deferred.resolve(data);
					}).error(function (data, status, headers, config) {
						deferred.reject(data);
					});
				return deferred.promise;
			}
		}
	}]);