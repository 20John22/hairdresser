﻿var module = angular.module("homeIndex", ['ngRoute', 'ngAnimate', 'ngSanitize', 'data-service']);

module.config(function ($routeProvider, $locationProvider) {
    $locationProvider
    .html5Mode(false)
    .hashPrefix('!');

    $routeProvider.when("/", {
        controller: "homeController",
        templateUrl: "/templates/about.html"
    });
    $routeProvider.when("/O-nas", {
        controller: "homeController",
        templateUrl: "/templates/about.html"
    });
    $routeProvider.when("/Oferta", {
        controller: "offerController",
        templateUrl: "/templates/offer.html"
    });
    $routeProvider.when("/Kontakt", {
        controller: "homeController",
        templateUrl: "/templates/contact.html"
    });
    $routeProvider.when("/Promocje", {
        controller: "homeController",
        templateUrl: "/templates/promotions.html"
    });

    //$routeProvider.otherwise({ redirectto: "/" });
})
function formatProduct(raw) {
	var formatedProduct			= new Object();
	formatedProduct.title		= raw.title;
	formatedProduct.description = raw.description;
	formatedProduct.src			= raw.url;
	//formatedProduct.processType = raw.processTypeInfo.code;
	formatedProduct.amount		= raw.minPrice + ' - ' + raw.maxPrice,
	formatedProduct.details		= { title: raw.details.title, content: raw.details.content }

	return formatedProduct;
}

module
	.controller('homeController', [function () { }])
	.controller('offerController', ['$scope', 'productService', function ($scope, productService) {
		
		/*
		var promise = productService.getAllProducts();

		//use Underscore.js for operations on list
		promise.then(function (result) {
			var offers = new Array();
			//create model for display needs
				//group result by process type (offer type)
					//process type
					//products: [ title, description, minprice, maxprice, details { title, content(allows html) }]
			for (var i = 0; i < result.length; i++) {
				var currentValue = result[i];
				
				var offer = _.findWhere(offers, { processType: currentValue.processTypeInfo.name }); // offers.where(o => o.ProcessType == currentValue.processType).FirstOrDefault();
				if (offer == undefined) {
					
					//create new offer
					offer = { processType: currentValue.processTypeInfo.name, products: [] };
					//add product to offer
					offer.products.push(formatProduct(currentValue));
					//add offer to offers list
					offers.push(offer);
				}
				else {
					//add product to offer
					offer.products.push(formatProduct(currentValue));
				}
			}
			$scope.offers = offers;
			$scope.setSelectedOffer($scope.offers[0]); //by default select offer 0
			$scope.setSelectedProduct($scope.offers[0].products[0]); //by default select product 0
		});
		*/
		
		$scope.offers = [
			{ 
				processType: '<br/>koloryzacja',
				products: [
					{ title: 'Nowa Kolekcja', description: '(Jesień, Zima 2016/2017)', amount: '145 - 175', src: '/Images/Offer/Koloryzja/Nowa Kolekcja.png', details: { title: 'Nowa Kolekcja', content: '' } },
					{ title: 'INOA', description: '(koloryzacja bez amoniaku)', amount: '125 - 150', src: '/Images/Offer/Koloryzja/INOA.png', details: { title: 'Inoa', content: '<ul><li>Udoskonalone pokrycie siwych włosów do 100%</li><li>6 tygodni intensywnego nawilżenia i odżywienia*</li><li>Bez zapachu, bez amoniaku</li><li>Optymalny komfort skóry głowy</li><li>Nieskończona moc koloru. Wyjątkowy połysk.</li></ul><h4>Technologia ODS^2(Oil Delivery System^2): </h4><p>Wyjątkowa technologia, która wykorzystuje moc olejku, aby w pełni zoptymalizować działanie kolorantów przy minimalnym stężeniu składnika zasadowego. Po raz pierwszy INOA, koloryzacja utleniająca bez amoniaku, gwarantująca wyjątkową pielęgnację włókna włosa** i optymalny komfort skóry głowy, dostępna jest w technologii 2-częściowej. Ponad 20 zgłoszonych patentów.</p>' } },
					{ title: 'INOA SUPREME', description: '(perfekcyjne pokrycie siwych włosów)', amount: '125 - 150', src: '/Images/Offer/Koloryzja/INOA SUPREME.png', details: { title: 'Inoa Supreme', content: '<ul><li>Perfekcyjne pokrycie siwych włosów, bez konieczności mieszania z bazą naturalną</li><li>Bez zapachu, bez amoniaku</li><li>Zwiększona objętość włosów. Podwójny refleks dla podkreślenia koloru cery</li><li>Optymalny komfort skóry głowy</li></ul><h4>Technologia ODS*: </h4><p>Nowatorski system na bazie olejku, zwiększający efektywnośćkoloryzacji. Pomaga zachować naturalną, ochronną warstwę lipidową włosa. <br>Inoa Supreme wzmacnia włosy, a kompleks Densilum sprawia, że włosy zystkują na objętości</p>' } },
					{ title: 'DIA LIGHT', description: '(kwasowy żel krem koloryzujący ton w ton bez amoniaku)', amount: '115 - 135', src: '/Images/Offer/Koloryzja/DIA LIGHT.png', details: { title: 'DIA LIGHT', content: ' <h4>Technologia kwasowa:</h4><ul><li> uwrażliwionych i koloryzowanych włosów. </li></ul><h4>Korzyści:</h4><ul><li>Zero rozjaśnienia</li><li>Trwały i jednolity kolor</li><li>Lustrzany połysk i świetlne refleksy</li><li>Efekt wygładzonej powierzchni włosa, jak po intensywnej pielęgnacji</li></ul><h4>Działanie:</h4> <p>W uwrażliwonym włóknie włosa łuski są rozchylone. Włókno włosa delikatnie pęcznieje w wodnym środowisku z kwasowym pH.Koloryzanty wnikają do środka włosa i worzijają się w harmonii z już istniejącymi kolorami we włóknie<p>' } },
					{ title: 'DIA RICHESSE', description: '(krem koloryzujący ton w ton bez amoniaku)', amount: '115 - 135', src: '/Images/Offer/Koloryzja/DIA RICHESSE.png', details: { title: 'DIA RICHESSE', content: '<h4>Technologia zasadowa:</h4><ul><li>naturalnych włosów.</li></ul><h4>Korzyści:</h4><ul><li>Pokrycie do 70% siwych włosów</li><li>Rozświetlenie do 1,5 tonu</li><li>Nasycone i głebokie refleksy</li><li>Wyjątkowa miękkość.</li></ul><h4>Działanie:</h4><p>W naturalnym włóknie włosa łuski do siebie przylegają.Pod wpływem czynnika zasadowego włókno włosa pęcznieje. Koloranty wnikają we włókno włosa i rozwijają się.</p>' } },
					{ title: 'MAJIREL', description: '(pielęgnacyjny krem koloryzujący)', amount: '105 - 125', src: '/Images/Offer/Koloryzja/MAJIREL.png', details: { title: 'Majirel', content: '<ul><li>Pielęgnujący krem koloryzujący</li><li>Nieograniczone możliwości kreacji kolorem</li><li>Pielęgnacja włosów w trakcie i po koloryzacji</li></ul><h4>Właściwości: </h4><ul><li>kompleks lonene G + Incell: pielęgnacja całego włokna włosa</li><li>Wyjątkowa trwałość dzięki systemowi przedłużonej trwałości HT</li><li>Rezultat koloryzacji jest intensywny, jednolity, trwały, pełen blasku kolor od nasady po końcówki</li><li>100% pokrycia siwych włosów</li><li>Świeży, kwiatowy zapach o delikatnej nucie morskiej</li></ul>' } },
					{ title: 'Luo Color', description: '(krem żel koloryzujący)', amount: '120 - 130', src: '/Images/Offer/Koloryzja/Luo Color.png', details: { title: 'Luo Color', content: '<ul><li>Lekkość koloru</li><li>świelistość</li><li>ruchome refleksy</li></ul><h4>Właściwości: </h4><ul><li>Luo Color zapewnia naturalny połysk i wielowymiarowość koloru</li><li>Innowacja konsystencja, perłowy krem-żel: łatwa i szybka aplikacja</li><li>Połączenie składników o działaniu odżywczm i dodającym blasku, technologia Nutrishine z olejkiem z pestek winogron</li><li>Rozjaśnianie: do 2.5-3 tonów</li><li>Pokrycie: do 70% siwych włosów</li></ul>' } },
				]
			},
			{
				processType: 'kuracja<br/>w salonie',
				products: [
					{ title: 'Liss Ultime', description: '', amount: '35', src: '', details: { title: 'Liss Ultime', content: '<h4>Włosy niezdyscyplinowane</h4><ul><li>Wygładzone</li><li>Jedwabiste w dotyku</li><li>Elastyczne włosy</li></ul><h4>Technologia: </h4><ul><li>POLIMER AR; podwójne działanie: miękkość i ochrona przed wilgocią. Polimer AR przeciwdziała puszeniu się włosów.</li><li>Olejek Arganowy; bogaty w witaminę E + Oliwa z oliwek: odżywienie, wygładzenie i połysk włosów.</li></ul><h4>Rezultat: </h4><p>Wygładzenie i połysk włosów.</p>' } },
					{ title: 'PRO-KERATIN REFILL', description: '(technologia innowacja)', amount: '45 - 55', src: '/Images/Offer/Kuracja/PRO KERATIN REFILL.png', details: { title: 'PRO-KERATIN REFILL', content: '<h4>Rodzaj włosów:</h4><p>do każdego rodzaju włosów.</p><h4>Efekt:</h4>  wypełnienie włókna włosa, wzmocnienie naturalnej ochrony włosa, niezwykle odporne włókna do 70%' } },
					{ title: 'Cristalceutic', description: '(technologia innowacja)', amount: '40 - 50', src: '/Images/Offer/Kuracja/Cristalceutic.png', details: { title: 'CRISTALCEUTIC', content: '<p>Usługa pielęgnacji koloru w salonie w dniu koloryzacji<br>Rodzaj włosów: włosy koloryzowane</p><h4>Efekt:</h4><p>Cristalceutic to pierwsza metoda „krystalizacji” koloru aby zatrzymać jego blask na dłużej. 6 tygodni krystalicznego blasku aby kolor wyglądał jak w dniu koloryzacji</p>' } },
					{ title: 'Absolut repair cellular', description: '(włosy bardzo uwrażliwione)', amount: '35 - 45', src: '/Images/Offer/Kuracja/Absolut repair cellular.png', details: { title: 'Absolut Repair Cellular', content: '<h4>Włosy bardzo uwrażliwione</h4><ul><li>Odbudowa</li><li>Regeneracja</li><li>Łatwość rozczesywania</li></ul><h4>Technologia: </h4><ul><li>KWAS MLEKOWY; odbudowuje połaczenia jonowe, przez co komórki kory zbliżają się do siebie tworząc wzmocnione i odbudowane wnętrze.</li><li>NEOFIBRINE; unikalne połączenie ceramidu Bio-Minetic, składników nadających połysk i filtru UV.</li></ul><h4>Rezultat: </h4><p>Włosy odzyskują naturalną witalność. <br>Włosy wyrażnie mocniejsze, lśniące i jedwabisćie gładkie.</p>' } },
					{ title: 'Vitamino Color POWERDOSE ', description: '(włosy koloryzowane)', amount: '35 - 45', src: '/Images/Offer/Kuracja/Vitamino Color POWERDOSE.png', details: { title: 'Vitamino Color Powerdose', content: '<h4>Włosy koloryzowane</h4><ul><li>przedłuzenie trwałości koloru</li><li>podwójna ochrona koloru</li><li>nadanie połysku</li></ul><h4>Technologia:</h4><ul><li>HYDRO-RESIST; system podwójnej ochrony włókna włosa; 1-sza ochrona - strefa A; hydro-resist odpycha wodę.</li><li>INCELL; 2-ga ochrona - strefa B; incell wzmacnia włókno włosa i zwiększa jego odporność.</li></ul>' } },
					{ title: 'Lumino Contrast', description: '(włosy z pasemkami)', amount: '35 - 45', src: '/Images/Offer/Kuracja/Lumino Contrast.png', details: { title: 'Lumino Contrast', content: '<h4>Włosy z pasemkami</h4><ul><li>Rozświetlone pasemka</li><li>Ochrona zasobów lipidowych</li><li>Podkreślenie kontrastu pasemek</li></ul><h4>Technologia:</h4><ul><li>NUTRICERIDE; Ceramidy i odżywcze olejki. Efekt synergii z Platinium.</li></ul><h4>Rezultat:</h4><p>Wygładzone i rozświetlone pasemka.<br> Włosy stają się jedwabiście miękkie i naturalne w dotyku.</p>' } },
					{ title: 'Fiberceutic', description: '(natychmiastowa odbudowa włókna włosa)', amount: '65 - 75', src: '/Images/Offer/Kuracja/Fiberceutic.png', details: { title: 'Fiberceutic', content: '<h4>Włosy bardzo zniszczone(zabiegami chemicznymi)</h4><ul><li>Natychmiastowa odbudowa włókna włosa</li></ul><h4>Technologia:</h4><ul><li>INTRA-CYLANE; Wzmacniawewnętrzną strukturę włókna włosa i uzupełnia jego ubytki. Cząsteczkadziałająca na rdzeń włosa.</li></ul><h4>Rezultat:</h4><p>Rekonstrukcja wewnetrznej strukuty <br>włokna włosa, długtrwały efekt odbudowy.</p>' } },
				]
			},
			{
				processType: '<br/>stylizacja',
				products: [
					{ title: 'Modelowanie', description: '(mycie + stylizacja)', amount: '45 - 65', src: '/Images/Offer/Sytlizacja/Modelowanie.jpg', details: { title: 'Modelowanie', content: '<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>' } },
					{ title: 'Strzyżenie', description: '(mycie + strzyżenie + stylizacja)', amount: '80 - 100', src: '/Images/Offer/Sytlizacja/Strzyzenie.png', details: { title: 'Strzyżenie', content: '<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>' } },
					{ title: 'Fryzura okolicznościowa', description: '(ślubna, sesyjna, kok)', amount: '105 - 155', src: '/Images/Offer/Sytlizacja/Fryzura okolicznosciowa.jpg', details: { title: 'Fryzura okolicznościowa', content: '<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>' } },
					{ title: 'Prostowanie', description: '', amount: '25 - 45', src: '/Images/Offer/Sytlizacja/Prostowanie.png', details: { title: 'Prostowanie', content: '<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>' } },
					{ title: 'Strzyżenie dziewcznyki', description: '(do 10 lat)', amount: '45', src: '/Images/Offer/Sytlizacja/Strzyzenie dziewcznyki.png', details: { title: 'Strzyżenie dziewcznyki', content: '<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>' } },
					{ title: 'Strzyżenie grzywki', description: '', amount: '20', src: '/Images/Offer/Sytlizacja/Strzyzenie grzywki.png', details: { title: 'Strzyżenie grzywki   ', content: '<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>' } },
				]
			},
			{
				processType: 'trwała<br/>zmiana formy',
				products: [
					{ title: 'X-Tenso Moisturist', description: '(długotrwałe prostowanie włosów)', amount: '125 - 165', src: '/Images/Offer/TrwalaZmianaFormy/XTenso Moisturist.png', details: { title: 'X-Tenso Moisturist:', content: '<ul><li>Długotrwałe prostowanie włosów</li><li>Wyjątkowe właściwości pielęgnacyjne</li><li>Trwałość do 8 tygodni*</li><li>Technologia Nutri-Cationic</li></ul><h4>Kremy X-Tenso Moisturist w naszym Salonie: </h4><ul><li>Włosy naturalne, oporne</li><li>Włosy naturalne</li><li>Włosy uwrażliwione</li></ul><h4>Technologia:</h4><p>NUTRI-CATIONIC; Kationowy składnik aktywny - molekuła, która posiada zdolność osadzania się na ujemnie naładowanych, uszkodzonych, partiach włosa. Jego właściwości odżywcze sprawiają, że włosy stają się gładkie, miekkie i łatwiejsze do wyprostowania WOSK ZMIĘKCZAJĄCY, aby zwiększyć gładkość i miękkość włosów.</p>' } },
					{ title: 'Dulcia Advanced', description: '(wzmacniająca trwała ondulacja)', amount: '105 - 130', src: '/Images/Offer/TrwalaZmianaFormy/Dulcia Advanced.png', details: { title: 'Dulcia Advanced', content: '<h4>4 Rodzaje Dulcia Advanced w naszym Salonie: </h4><ul><li>Włosy naturalne, oporne na skręt</li><li>Włosy naturalne</li><li>Włosy uwrażliwione</li><li>Włosy bardzo uwrażliwione</li></ul><h4>Właściwości i Zalety: </h4><ul><li>Wzmacniająca trwała ondulacja</li><li>lonene G, aby chronić włókno włosa i poprawiać jego kondycję podczas trwałej onducji.</li><li>Powierzchnia włosa zostaje pokryta ochronną warstwą, aby zapobiegać utracie wilgoci oraz zapewnić gładkość włókna włosa i ułatwić rozczesywanie.</li></ul>' } },
				]
			},
			{
				processType: 'modyfikacja<br/>koloru',
				products: [
					{ title: 'Duo Color', description: '', amount: '145 - 175', src: '/Images/Offer/ModyfikacjaKoloru/Duo Color.jpg', details: { title: 'Duo Color', description: '' } },
					{ title: 'Balejaż', description: '(technika doskonale naśladująca naturalne rozjaśnienie słońcem)', amount: '125 - 155', src: '/Images/Offer/ModyfikacjaKoloru/Balejaz.jpg', details: { title: 'Balejaż', content: '' } },
					{ title: 'Balejaż Duo', description: '(indywidalne i bardzo twarzowe rezultaty)', amount: '145 - 175', src: '/Images/Offer/ModyfikacjaKoloru/Balejaz Duo.jpg', details: { title: 'Balejaż Duo', content: '' } },
					{ title: 'Majicontrast', description: '(balejaż dla brunetek)', amount: '65 - 155', src: '/Images/Offer/ModyfikacjaKoloru/Majicontrast.jpg', details: { title: 'Majicontrast', content: '<ul><li>Balejaż dla brunetek</li></ul><h4>Właściwości: </h4><ul><li>Formuła zawierająca pigmenty Hi.chroma, aby nasycić włosy kolorem</li><li>3 odcienie: czerwony, czerwono-opalizujący, miedziany - "Balejaż dla brunetek"</li></ul>' } },
					{ title: 'Demakijaż', description: '(korektor pianowy do demakijażu i modyfikacji)', amount: '145 - 185', src: '/Images/Offer/ModyfikacjaKoloru/Demakijaz.jpg', details: { title: 'Demakijaż', content: '' } },
				] }
		];
		
		$scope.selectedOffer = '';
		$scope.selectedProduct = '';

		$scope.isActiveProduct = function (product) {
			return $scope.selectedProduct === product;
		};
		$scope.isActive = function (offer) {
			return $scope.selectedOffer === offer;
		};

		$scope.setSelectedProduct = function (product) {
			$scope.selectedProduct				= product;
		};
		$scope.setSelectedOffer = function (offer) {
			$scope.selectedOffer	= offer;
		};

		$scope.setSelectedOffer($scope.offers[0]); //by default select offer 0
		$scope.setSelectedProduct($scope.offers[0].products[0]); //by default select product 0

}]);