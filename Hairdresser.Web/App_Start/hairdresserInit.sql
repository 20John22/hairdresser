use [Hairdresser.Entities.CoreContext];

-- teProcessType
--Id	Code
--1	Koloryzja
--2	Sytlizacja
--3	Kuracja
--4	ModyfikacjaKoloru
--5	TrwalaZmianaFormy

INSERT INTO tDetails (Title, Content)
VALUES ('Nowa Kolekcja','');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Nowa Kolekcja','(Jesień, Zima 2013/2014)','/Images/Offer/Koloryzja/Nowa Kolekcja.png',140, 170,  @@IDENTITY, 1);

INSERT INTO tDetails (Title, Content)
VALUES ('Inoa','<ul><li>Udoskonalone pokrycie siwych włosów do 100%</li><li>6 tygodni intensywnego nawilżenia i odżywienia*</li><li>Bez zapachu, bez amoniaku</li><li>Optymalny komfort skóry głowy</li><li>Nieskończona moc koloru. Wyjątkowy połysk.</li></ul><h4>Technologia ODS^2(Oil Delivery System^2): </h4><p>Wyjątkowa technologia, która wykorzystuje moc olejku, aby w pełni zoptymalizować działanie kolorantów przy minimalnym stężeniu składnika zasadowego. Po raz pierwszy INOA, koloryzacja utleniająca bez amoniaku, gwarantująca wyjątkową pielęgnację włókna włosa** i optymalny komfort skóry głowy, dostępna jest w technologii 2-częściowej. Ponad 20 zgłoszonych patentów.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('INOA','(koloryzacja bez amoniaku)','/Images/Offer/Koloryzja/INOA.png',120, 145,  @@IDENTITY, 1);

INSERT INTO tDetails (Title, Content)
VALUES ('Inoa Supreme','<ul><li>Perfekcyjne pokrycie siwych włosów, bez konieczności mieszania z bazą naturalną</li><li>Bez zapachu, bez amoniaku</li><li>Zwiększona objętość włosów. Podwójny refleks dla podkreślenia koloru cery</li><li>Optymalny komfort skóry głowy</li></ul><h4>Technologia ODS*: </h4><p>Nowatorski system na bazie olejku, zwiększający efektywnośćkoloryzacji. Pomaga zachować naturalną, ochronną warstwę lipidową włosa. <br>Inoa Supreme wzmacnia włosy, a kompleks Densilum sprawia, że włosy zystkują na objętości</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('INOA SUPREME','(perfekcyjne pokrycie siwych włosów)','/Images/Offer/Koloryzja/INOA SUPREME.png',120, 145,  @@IDENTITY, 1);

INSERT INTO tDetails (Title, Content)
VALUES ('DIA LIGHT','<h4>Technologia kwasowa:</h4><ul><li> uwrażliwionych i koloryzowanych włosów. </li></ul><h4>Korzyści:</h4><ul><li>Zero rozjaśnienia</li><li>Trwały i jednolity kolor</li><li>Lustrzany połysk i świetlne refleksy</li><li>Efekt wygładzonej powierzchni włosa, jak po intensywnej pielęgnacji</li></ul><h4>Działanie:</h4> <p>W uwrażliwonym włóknie włosa łuski są rozchylone. Włókno włosa delikatnie pęcznieje w wodnym środowisku z kwasowym pH.Koloryzanty wnikają do środka włosa i worzijają się w harmonii z już istniejącymi kolorami we włóknie<p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('DIA LIGHT','(kwasowy żel krem koloryzujący ton w ton bez amoniaku)','/Images/Offer/Koloryzja/DIA LIGHT.png',110, 130,  @@IDENTITY, 1);

INSERT INTO tDetails (Title, Content)
VALUES ('DIA RICHESSE','<h4>Technologia zasadowa:</h4><ul><li>naturalnych włosów.</li></ul><h4>Korzyści:</h4><ul><li>Pokrycie do 70% siwych włosów</li><li>Rozświetlenie do 1,5 tonu</li><li>Nasycone i głebokie refleksy</li><li>Wyjątkowa miękkość.</li></ul><h4>Działanie:</h4><p>W naturalnym włóknie włosa łuski do siebie przylegają.Pod wpływem czynnika zasadowego włókno włosa pęcznieje. Koloranty wnikają we włókno włosa i rozwijają się.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('DIA RICHESSE','(krem koloryzujący ton w ton bez amoniaku)','/Images/Offer/Koloryzja/DIA RICHESSE.png',110, 130,  @@IDENTITY, 1);

INSERT INTO tDetails (Title, Content)
VALUES ('Majirel','<ul><li>Pielęgnujący krem koloryzujący</li><li>Nieograniczone możliwości kreacji kolorem</li><li>Pielęgnacja włosów w trakcie i po koloryzacji</li></ul><h4>Właściwości: </h4><ul><li>kompleks lonene G + Incell: pielęgnacja całego włokna włosa</li><li>Wyjątkowa trwałość dzięki systemowi przedłużonej trwałości HT</li><li>Rezultat koloryzacji jest intensywny, jednolity, trwały, pełen blasku kolor od nasady po końcówki</li><li>100% pokrycia siwych włosów</li><li>Świeży, kwiatowy zapach o delikatnej nucie morskiej</li></ul>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('MAJIREL','(pielęgnacyjny krem koloryzujący)','/Images/Offer/Koloryzja/MAJIREL.png',100, 120,  @@IDENTITY, 1);

INSERT INTO tDetails (Title, Content)
VALUES ('Luo Color','<ul><li>Lekkość koloru</li><li>świelistość</li><li>ruchome refleksy</li></ul><h4>Właściwości: </h4><ul><li>Luo Color zapewnia naturalny połysk i wielowymiarowość koloru</li><li>Innowacja konsystencja, perłowy krem-żel: łatwa i szybka aplikacja</li><li>Połączenie składników o działaniu odżywczm i dodającym blasku, technologia Nutrishine z olejkiem z pestek winogron</li><li>Rozjaśnianie: do 2.5-3 tonów</li><li>Pokrycie: do 70% siwych włosów</li></ul>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Luo Color','(krem żel koloryzujący)','/Images/Offer/Koloryzja/Luo Color.png',115, 125, @@IDENTITY, 1);

-- teProcessType
--Id	Code
--1	Koloryzja
--2	Sytlizacja
--3	Kuracja
--4	ModyfikacjaKoloru
--5	TrwalaZmianaFormy

INSERT INTO tDetails (Title, Content)
VALUES ('Liss Ultime','<h4>Włosy niezdyscyplinowane</h4><ul><li>Wygładzone</li><li>Jedwabiste w dotyku</li><li>Elastyczne włosy</li></ul><h4>Technologia: </h4><ul><li>POLIMER AR; podwójne działanie: miękkość i ochrona przed wilgocią. Polimer AR przeciwdziała puszeniu się włosów.</li><li>Olejek Arganowy; bogaty w witaminę E + Oliwa z oliwek: odżywienie, wygładzenie i połysk włosów.</li></ul><h4>Rezultat: </h4><p>Wygładzenie i połysk włosów.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Liss Ultime','(technologia innowacja)','',40, 50,  @@IDENTITY, 3);

INSERT INTO tDetails (Title, Content)
VALUES ('PRO-KERATIN REFILL','<h4>Rodzaj włosów:</h4><p>do każdego rodzaju włosów.</p><h4>Efekt:</h4>  wypełnienie włókna włosa, wzmocnienie naturalnej ochrony włosa, niezwykle odporne włókna do 70%');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('PRO-KERATIN REFILL','','/Images/Offer/Kuracja/PRO-KERATIN REFILL.png',40, 45,  @@IDENTITY, 3);

INSERT INTO tDetails (Title, Content)
VALUES ('CRISTALCEUTIC','<p>Usługa pielęgnacji koloru w salonie w dniu koloryzacji<br>Rodzaj włosów: włosy koloryzowane</p><h4>Efekt:</h4><p>Cristalceutic to pierwsza metoda „krystalizacji” koloru aby zatrzymać jego blask na dłużej. 6 tygodni krystalicznego blasku aby kolor wyglądał jak w dniu koloryzacji</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Cristalceutic','(technologia innowacja)','/Images/Offer/Kuracja/Cristalceutic.png',35, 45,  @@IDENTITY, 3);

INSERT INTO tDetails (Title, Content)
VALUES ('Absolut Repair Cellular','<h4>Włosy bardzo uwrażliwione</h4><ul><li>Odbudowa</li><li>Regeneracja</li><li>Łatwość rozczesywania</li></ul><h4>Technologia: </h4><ul><li>KWAS MLEKOWY; odbudowuje połaczenia jonowe, przez co komórki kory zbliżają się do siebie tworząc wzmocnione i odbudowane wnętrze.</li><li>NEOFIBRINE; unikalne połączenie ceramidu Bio-Minetic, składników nadających połysk i filtru UV.</li></ul><h4>Rezultat: </h4><p>Włosy odzyskują naturalną witalność. <br>Włosy wyrażnie mocniejsze, lśniące i jedwabisćie gładkie.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Absolut repair cellular','(włosy bardzo uwrażliwione)','/Images/Offer/Kuracja/Absolut repair cellular.png',30, 40,  @@IDENTITY, 3);

INSERT INTO tDetails (Title, Content)
VALUES ('Vitamino Color Powerdose','<h4>Włosy koloryzowane</h4><ul><li>przedłuzenie trwałości koloru</li><li>podwójna ochrona koloru</li><li>nadanie połysku</li></ul><h4>Technologia:</h4><ul><li>HYDRO-RESIST; system podwójnej ochrony włókna włosa; 1-sza ochrona - strefa A; hydro-resist odpycha wodę.</li><li>INCELL; 2-ga ochrona - strefa B; incell wzmacnia włókno włosa i zwiększa jego odporność.</li></ul>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Vitamino Color POWERDOSE','(włosy koloryzowane)','/Images/Offer/Kuracja/Vitamino Color POWERDOSE.png',30, 40,  @@IDENTITY, 3);

INSERT INTO tDetails (Title, Content)
VALUES ('Lumino Contrast','<h4>Włosy z pasemkami</h4><ul><li>Rozświetlone pasemka</li><li>Ochrona zasobów lipidowych</li><li>Podkreślenie kontrastu pasemek</li></ul><h4>Technologia:</h4><ul><li>NUTRICERIDE; Ceramidy i odżywcze olejki. Efekt synergii z Platinium.</li></ul><h4>Rezultat:</h4><p>Wygładzone i rozświetlone pasemka.<br> Włosy stają się jedwabiście miękkie i naturalne w dotyku.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Lumino Contrast','(włosy z pasemkami)','/Images/Offer/Kuracja/Lumino Contrast.png',30, 40,  @@IDENTITY, 3);

INSERT INTO tDetails (Title, Content)
VALUES ('Fiberceutic','<h4>Włosy bardzo zniszczone(zabiegami chemicznymi)</h4><ul><li>Natychmiastowa odbudowa włókna włosa</li></ul><h4>Technologia:</h4><ul><li>INTRA-CYLANE; Wzmacniawewnętrzną strukturę włókna włosa i uzupełnia jego ubytki. Cząsteczkadziałająca na rdzeń włosa.</li></ul><h4>Rezultat:</h4><p>Rekonstrukcja wewnetrznej strukuty <br>włokna włosa, długtrwały efekt odbudowy.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Fiberceutic','(natychmiastowa odbudowa włókna włosa)','/Images/Offer/Kuracja/Fiberceutic.png',60, 70,  @@IDENTITY, 3);

-- teProcessType
--Id	Code
--1	Koloryzja
--2	Sytlizacja
--3	Kuracja
--4	ModyfikacjaKoloru
--5	TrwalaZmianaFormy

INSERT INTO tDetails (Title, Content)
VALUES ('Modelowanie','<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Modelowanie','(mycie + stylizacja)','/Images/Offer/Sytlizacja/Modelowanie.jpg',40, 60,  @@IDENTITY, 2);

INSERT INTO tDetails (Title, Content)
VALUES ('Strzyżenie','<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Strzyżenie','(mycie + strzyżenie + stylizacja)','/Images/Offer/Sytlizacja/Strzyżenie.png',75, 95,  @@IDENTITY, 2);

INSERT INTO tDetails (Title, Content)
VALUES ('Fryzura okolicznościowa','<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Fryzura okolicznościowa','(ślubna, sesyjna, kok)','/Images/Offer/Sytlizacja/Fryzura okolicznościowa.jpg',100, 150,  @@IDENTITY, 2);

INSERT INTO tDetails (Title, Content)
VALUES ('Prostowanie','<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Prostowanie','','/Images/Offer/Sytlizacja/Prostowanie.png',20, 40,  @@IDENTITY, 2);

INSERT INTO tDetails (Title, Content)
VALUES ('Strzyżenie dziewcznyki','<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Strzyżenie dziewcznyki','(do 10 lat)','/Images/Offer/Sytlizacja/Strzyżenie dziewcznyki.png',40, 40,  @@IDENTITY, 2);

INSERT INTO tDetails (Title, Content)
VALUES ('Strzyżenie grzywki','<h4>Pełna dbałośćo włosy</h4><p>Wysokiej jakości, lekkie i nieklejące się konsystencje, które nie pozostawiają osadu i łatwo się wyczesują.</p><h4>Doskonały efekt </h4><p>Te Łatwe w stosowaniu produkty, umożliwiają profesjonalne układanie włosów.<br> Bez względu na potrzebę danej chwili, sposobu utrwalenia, efektu objętości, wygładzenia czy połysku, tecni.art to proste rozwiązanie na stworzenie dowolnego wizerunku.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Strzyżenie grzywki','','/Images/Offer/Sytlizacja/Strzyżenie grzywki.png',15, 15,  @@IDENTITY, 2);

-- teProcessType
--Id	Code
--1	Koloryzja
--2	Sytlizacja
--3	Kuracja
--4	ModyfikacjaKoloru
--5	TrwalaZmianaFormy

INSERT INTO tDetails (Title, Content)
VALUES ('X-Tenso Moisturist','<ul><li>Długotrwałe prostowanie włosów</li><li>Wyjątkowe właściwości pielęgnacyjne</li><li>Trwałość do 8 tygodni*</li><li>Technologia Nutri-Cationic</li></ul><h4>Kremy X-Tenso Moisturist w naszym Salonie: </h4><ul><li>Włosy naturalne, oporne</li><li>Włosy naturalne</li><li>Włosy uwrażliwione</li></ul><h4>Technologia:</h4><p>NUTRI-CATIONIC; Kationowy składnik aktywny - molekuła, która posiada zdolność osadzania się na ujemnie naładowanych, uszkodzonych, partiach włosa. Jego właściwości odżywcze sprawiają, że włosy stają się gładkie, miekkie i łatwiejsze do wyprostowania WOSK ZMIĘKCZAJĄCY, aby zwiększyć gładkość i miękkość włosów.</p>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('X-Tenso Moisturist','(długotrwałe prostowanie włosów)','/Images/Offer/TrwalaZmianaFormy/X-Tenso Moisturist.png',120, 160,  @@IDENTITY, 5);

INSERT INTO tDetails (Title, Content)
VALUES ('Dulcia Advanced','<h4>4 Rodzaje Dulcia Advanced w naszym Salonie: </h4><ul><li>Włosy naturalne, oporne na skręt</li><li>Włosy naturalne</li><li>Włosy uwrażliwione</li><li>Włosy bardzo uwrażliwione</li></ul><h4>Właściwości i Zalety: </h4><ul><li>Wzmacniająca trwała ondulacja</li><li>lonene G, aby chronić włókno włosa i poprawiać jego kondycję podczas trwałej onducji.</li><li>Powierzchnia włosa zostaje pokryta ochronną warstwą, aby zapobiegać utracie wilgoci oraz zapewnić gładkość włókna włosa i ułatwić rozczesywanie.</li></ul>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Dulcia Advanced','(wzmacniająca trwała ondulacja)','/Images/Offer/TrwalaZmianaFormy/Dulcia Advanced.png',100, 125,  @@IDENTITY, 5);

-- teProcessType
--Id	Code
--1	Koloryzja
--2	Sytlizacja
--3	Kuracja
--4	ModyfikacjaKoloru
--5	TrwalaZmianaFormy

INSERT INTO tDetails (Title, Content)
VALUES ('Duo Color','');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Duo Color','','/Images/Offer/ModyfikacjaKoloru/Duo Color.jpg',140, 170,  @@IDENTITY, 4);

INSERT INTO tDetails (Title, Content)
VALUES ('Balejaż','');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Balejaż','(technika doskonale naśladująca naturalne rozjaśnienie słońcem)','/Images/Offer/ModyfikacjaKoloru/Balejaż.jpg',120, 150,  @@IDENTITY, 4);

INSERT INTO tDetails (Title, Content)
VALUES ('Balejaż Duo','');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Balejaż Duo','(indywidalne i bardzo twarzowe rezultaty)','/Images/Offer/ModyfikacjaKoloru/Balejaż Duo.jpg',140, 170,  @@IDENTITY, 4);

INSERT INTO tDetails (Title, Content)
VALUES ('Majicontrast','<ul><li>Balejaż dla brunetek</li></ul><h4>Właściwości: </h4><ul><li>Formuła zawierająca pigmenty Hi.chroma, aby nasycić włosy kolorem</li><li>3 odcienie: czerwony, czerwono-opalizujący, miedziany - "Balejaż dla brunetek"</li></ul>');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Majicontrast','(balejaż dla brunetek)','/Images/Offer/ModyfikacjaKoloru/Majicontrast.jpg',60, 150,  @@IDENTITY, 4);

INSERT INTO tDetails (Title, Content)
VALUES ('Demakijaż','');
INSERT INTO tProduct (Title,Description,Url,MinPrice,MaxPrice,DetailsId,ProcessTypeId) 
VALUES ('Demakijaż','(korektor pianowy do demakijażu i modyfikacji)','/Images/Offer/ModyfikacjaKoloru/Demakijaż.jpg',140, 180,  @@IDENTITY, 4);