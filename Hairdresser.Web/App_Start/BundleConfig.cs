﻿using System.Web.Optimization;

namespace Hairdresser.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-route.js",
                        "~/Scripts/angular-animate.js",
                        "~/Scripts/angular-sanitize.js",
						"~/Scripts/underscore.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Content/bootstrap/app/bootstrap-app.min.css",
                "~/Content/normalize.css", 
                "~/Content/site.min.css", 
                "~/Content/menu.css", 
                "~/Content/promotions.css", 
                "~/Content/animation.css", 
                "~/Content/offer.css"   
            ));

            //BundleTable.EnableOptimizations = true;
        }
    }
}