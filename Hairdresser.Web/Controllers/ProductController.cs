﻿using System.Collections.Generic;
using System.Web.Http;
using System.Linq;

using Hairdresser.Entities;
using Hairdresser.Model;
using WebApi.OutputCache.V2.TimeAttributes;

namespace Hairdresser.WebApi.Controllers
{
	public class ProductController : ApiController
	{
		private readonly CoreContext _coreContext;

		public ProductController()
		{
			_coreContext = new CoreContext("hairdresserCS");
		}

		[HttpGet]
        [CacheOutputUntilThisMonth(1)]
		public IEnumerable<Product> Get()
		{
			var result = _coreContext.Products.ToList();
            return result;
		}

	}
}
