﻿
namespace Hairdresser.Model
{
	public class Details
	{
		public virtual int Id { get; set; }
		public virtual string Title { get; set; }
		/// <summary>
		/// Allows html tags
		/// </summary>
		public virtual string Content { get; set; }
	}
}
