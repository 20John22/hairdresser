﻿using System;

namespace Hairdresser.Model
{
	public enum ProcessType
	{
		Koloryzja = 1,
		Sytlizacja = 2,
		Kuracja = 3,
		ModyfikacjaKoloru = 4,
		TrwalaZmianaFormy = 5
	}

	public class ProcessTypeInfo
	{
		public ProcessTypeInfo Initialize(int id, string code, string name)
		{
			return Initialize(
				(ProcessType)Enum.ToObject(typeof(ProcessType), id),
				code,
				name
				);
		}
		public ProcessTypeInfo Initialize(ProcessType value, string code, string name)
		{
			Id			= value;
			Code		= code;
			Name		= name;
			return this;
		}

		#region Properties

		public virtual ProcessType Id
		{
			get;
			protected set;
		}

		public virtual string Code
		{
			get;
			protected set;
		}

		public virtual string Name
		{
			get;
			protected set;
		}

		#endregion
	}
}
