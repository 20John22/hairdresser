﻿
namespace Hairdresser.Model
{
	public class Product
	{
		public virtual int Id { get; set; }
		public virtual string Title { get; set; }
		public virtual string Description { get; set; }
		public virtual decimal MinPrice { get; set; }
		public virtual decimal MaxPrice { get; set; }
		public virtual string Url { get; set; }

		public virtual int DetailsId { get; set; }
		public virtual Details Details { get; set; }

		public virtual ProcessType ProcessType { get; set; }
		public virtual ProcessTypeInfo ProcessTypeInfo { get; set; }
	}
}
